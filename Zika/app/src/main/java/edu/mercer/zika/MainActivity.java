package edu.mercer.zika;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void buttonClicked(View view){

        int id = view.getId();

        if (id == R.id.what_is_button_id){

            Intent intent = new Intent(this, InformationActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.symptom_checker_button_id){
            Intent intent = new Intent(this, Sympton_Check.class);
            startActivity(intent);
        }
        else if (id == R.id.risk_button_id){

            RiskAlertDialogFragment fragment = new RiskAlertDialogFragment();
            fragment.show(getSupportFragmentManager(), getString(R.string.tag));
        }
        else if (id == R.id.prevent_button_id){
            Intent intent = new Intent(this, PreventionActivity.class);
            startActivity(intent);
        }
        else {
            Intent intent = new Intent(this, TreatmentActivity.class);
            startActivity(intent);
        }
    }
}
