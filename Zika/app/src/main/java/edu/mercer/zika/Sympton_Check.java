package edu.mercer.zika;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class Sympton_Check extends AppCompatActivity {

    CheckBox headache, fever, rash, jointPain, redEyes, musclePain;
    Button getReslutBtn;
    Integer chanceOfZika;
    Bundle scoreBundle;
    RadioButton radioButtonYes;
    RadioGroup myRadioGrp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sympton__check);

        chanceOfZika = 0;
        getReslutBtn = (Button) findViewById(R.id.sympCheckSubmit);
        getReslutBtn.setOnClickListener(myOnclickListener);

        myRadioGrp = (RadioGroup) findViewById(R.id.rdGrp1);
        myRadioGrp.setOnCheckedChangeListener(myOnchangedListener);
        radioButtonYes = (RadioButton) findViewById(R.id.rdBtn1);
        radioButtonYes.setChecked(true);

        /*initializing checked boxes and adding the onclick method.*/

        headache = (CheckBox)findViewById(R.id.headache);
        headache.setOnClickListener(myOnclickListener);

        fever = (CheckBox)findViewById(R.id.fever);
        fever.setOnClickListener(myOnclickListener);

        rash = (CheckBox)findViewById(R.id.rash);
        rash.setOnClickListener(myOnclickListener);

        jointPain = (CheckBox)findViewById(R.id.Jpain);
        jointPain.setOnClickListener(myOnclickListener);

        redEyes = (CheckBox)findViewById(R.id.RedEyes);
        redEyes.setOnClickListener(myOnclickListener);

        musclePain = (CheckBox)findViewById(R.id.musclePain);
        musclePain.setOnClickListener(myOnclickListener);
    }

    private RadioGroup.OnCheckedChangeListener myOnchangedListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            if(radioButtonYes.isChecked()){
                chanceOfZika += 20;
            }else{
                chanceOfZika -=20;
            }
        }
    };


    private View.OnClickListener myOnclickListener = new View.OnClickListener(){

        @Override
        public void onClick(View v){
           // String toastMsg = "Checked";
            //beginning of 4 common symptoms
            if(v == fever){

                if( ((CheckBox)v).isChecked()) { //if checked, plus 15 else minus 15
                    chanceOfZika += 15;
                }else{
                    chanceOfZika -= 15;
                }

            }else if(v == rash){
                if( ((CheckBox)v).isChecked()) {
                    chanceOfZika += 15;
                }else{
                    chanceOfZika -= 15;
                }
            }
            else if(v == jointPain){
                if( ((CheckBox)v).isChecked()) {
                    chanceOfZika += 15;
                }else{
                    chanceOfZika -= 15;
                }

            }
            else if(v == redEyes){
                if( ((CheckBox)v).isChecked()) {
                    chanceOfZika += 15;
                }else{
                    chanceOfZika -= 15;
                }

            }//end of 4 common symptoms
             else if(v == headache){
                if( ((CheckBox)v).isChecked()) {
                    chanceOfZika += 10;
                }else{
                    chanceOfZika -= 10;
                }

            }else if(v == musclePain){
                if( ((CheckBox)v).isChecked()) {
                    chanceOfZika += 10;

                }else{
                    chanceOfZika -= 10;
                }

            }else if(v == getReslutBtn ){ //submit result to the scoring page which shows the possibility of zika
                String score = chanceOfZika.toString();
                Intent intentForScore = new Intent(Sympton_Check.this, SymptonScore.class);
                intentForScore.putExtra(getString(R.string.scorekey), score);
                startActivity(intentForScore);
            }
        }

    };
}
