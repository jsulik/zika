package edu.mercer.zika;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

public class RiskAlertDialogFragment extends DialogFragment {

    public Dialog onCreateDialog (Bundle savedInstanceState) {

        //build alert dialog
        Dialog myDialog = null;
        myDialog = new AlertDialog.Builder(getActivity())

                .setTitle(getString(R.string.title_text))
                .setMessage(getString(R.string.query_text))
                .setPositiveButton(getString(R.string.positive_text), myAlertDialogListener)
                .setNegativeButton(getString(R.string.negative_text), myAlertDialogListener)
                .create();
        return myDialog;

    }

    private DialogInterface.OnClickListener myAlertDialogListener =
            new  DialogInterface.OnClickListener()  {

                @Override
                public void onClick(DialogInterface dialog, int whichButton) {

                    switch (whichButton) {
                        //continue to further info
                        case Dialog.BUTTON_POSITIVE:

                            Intent intent = new Intent(getContext(), RiskActivity.class);
                            startActivity(intent);
                            break;
                        //no need for further info
                        case Dialog.BUTTON_NEGATIVE:

                            Toast.makeText(getActivity().getBaseContext(),
                                    getString(R.string.negative_toast),
                                    Toast.LENGTH_LONG).show();
                            break;
                    }   // end of switch

                    dialog.dismiss();  // Dismiss Alert Dialog
                }
            };
}
