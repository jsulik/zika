package edu.mercer.zika;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import org.w3c.dom.Text;

public class TreatmentActivity extends AppCompatActivity {

    TextView hyperlink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treatment);

        //make hotlink an active link to web browser
        hyperlink = (TextView)findViewById(R.id.hyperlink_id);
        hyperlink.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
