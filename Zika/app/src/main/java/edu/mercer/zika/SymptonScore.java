package edu.mercer.zika;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class SymptonScore extends AppCompatActivity {
TextView displayScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sympton_score);

        displayScore = (TextView) findViewById(R.id.sympScore);
        Intent intent = getIntent();
        int theScore = Integer.parseInt(intent.getStringExtra(getString(R.string.scorekey)));
        String finalResult = "";

        if (theScore <=10){
            finalResult = getString(R.string.tenPercent);
        }else if(theScore > 10 && theScore < 50){
            finalResult = getString(R.string.little);
        }
        else if(theScore == 50){
            finalResult = getString(R.string.fiftyPercent);
        }else if(theScore > 50 && theScore <=70){
            finalResult = getString(R.string.belowsSeventy);
        }else if(theScore > 70 && theScore <=90){
            finalResult = getString(R.string.belowNinty);
        }else{
            finalResult = getString(R.string.hundred);
        }

        Toast.makeText(getBaseContext(), String.valueOf(theScore) + getString(R.string.possibility), Toast.LENGTH_LONG).show();
        displayScore.setText(finalResult);

    }
}
