package edu.mercer.zika;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.content.Intent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class RiskActivity extends AppCompatActivity {

    ImageView map;
    boolean isImageFitToScreen;
    ListView countryList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_risk);

        map = (ImageView) findViewById(R.id.map_id);

        //make map bigger if clicked
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isImageFitToScreen) {
                    isImageFitToScreen = false;
                    map.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    map.setAdjustViewBounds(true);
                } else {
                    isImageFitToScreen = true;
                    map.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                    map.setScaleType(ImageView.ScaleType.FIT_XY);
                }
            }
        });

        //initialize list
        countryList = (ListView) findViewById(R.id.country_list_id);
        ArrayList<String> countries = new ArrayList<>(Arrays.asList(
                getResources().getStringArray(R.array.country_array)
        ));

        Collections.sort(countries);

        ArrayAdapter<String> myAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, countries);

        countryList.setAdapter(myAdapter);
        countryList.setOnItemClickListener(countryClickListener);
    }

    private  ListView.OnItemClickListener   countryClickListener =
            new ListView.OnItemClickListener() {

                // The onItemClick() method is called whenever
                // an item in the ListView has been clicked
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    // ListView Clicked item value
                    String  itemValue    = (String) countryList.getItemAtPosition(position);

                    //create the proper lin based on the lciked value
                    String link = getString(R.string.country_cdc_link) + itemValue.replace(" ", "-");

                    //open browser using link
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                    startActivity(browserIntent);
                }

            };


}
